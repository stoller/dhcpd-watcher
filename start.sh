#!/bin/sh

DHCPD_WATCHER=/local/repository/dhcpd-watcher.pl
DHCPD_LOGFILE=/var/log/dhcpd-watcher.log

GATEWAY_WATCHER=/local/repository/gateway-watcher.pl
GATEWAY_LOGFILE=/var/log/gateway-watcher.log

DAEMON="daemon -a 60 -A 2 -L 3600 -U -N -r"

#
# If it dies with 30 seconds, call it quits.
#
sudo $DAEMON -n dhcpd-watcher -o $DHCPD_LOGFILE -- $DHCPD_WATCHER
if [ $? -ne 0 ]; then
    echo 'Could not start the DHCPD watcher'
    exit 1
fi
sudo $DAEMON -n gateway-watcher -o $GATEWAY_LOGFILE -- $GATEWAY_WATCHER
if [ $? -ne 0 ]; then
    echo 'Could not start the Gateway watcher'
    exit 1
fi

exit 0
