#!/usr/bin/perl -w
#
# Copyright (c) 2000-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use Getopt::Std;
use English;
use Errno;
use POSIX qw(strftime);

#
# Watch for a change in the gateway mac and report it.
#
sub usage()
{
    print "Usage: gateway-watcher.pl [-dn]\n";
    exit(1);
}
my $optlist   = "dns";
my $debug     = 0;
my $impotent  = 0;
my $oneshot   = 0;
my $iface;
my $routerip;
my $routermac;
my $vmroutermac;
my $domain;
my $tbops;

my $VMROUTER  = "172.16.0.1";
my $VMALIAS   = "172.17.253.128";
my $NMAP      = "/bin/nmap";
my $ARP       = "/sbin/arp";
my $IP	      = "/bin/ip";
my $PING      = "/bin/ping";

#
# Turn off line buffering on output
#
$| = 1;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Load the OS independent support library. It will load the OS dependent
# library and initialize itself.
#
use libsetup;
use libtmcc;
use libtestbed;

# Protos
sub GetMac($);
sub LockMac($);
sub fatal($);
sub notify($);
sub ExecQuiet($);

#
# Parse command arguments. Once we return from getopts, all that should be
# left are the required arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"h"})) {
    usage();
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"s"})) {
    $oneshot++;
}

# For sending the email alerts through the local boss.
my ($bossname,$bossip) = libtmcc::tmccbossinfo();
my $opsname = $bossname =~ s/boss/ops/r;

if (-e "$BOOTDIR/mydomain") {
    $domain = `cat $BOOTDIR/mydomain`;
    chomp($domain);
}
else {
    fatal("Could not get the local domain");
}

# Local testbed-ops forwards to Utah
if (!$debug) {
    $tbops = "testbed-ops\@${opsname}";
}
else {
    $tbops = "stoller\@ops.emulab.net";
}

# Figure out the control net interface for nmap
if (-e "$BOOTDIR/controlif") {
    $iface = `cat $BOOTDIR/controlif`;
    chomp($iface);
}
else {
    fatal("Could not get the control interface");
}

#
# Get the current MAC for the gateway, if its wrong to start then we
# probably are going to fail pretty quickly.
#
if (-e "$BOOTDIR/routerip") {
    $routerip = `cat $BOOTDIR/routerip`;
    chomp($routerip);
}
else {
    fatal("Could not get the router IP");
}
$routermac = LockMac($routerip);
print "Gateway IP: $routerip, Gateway MAC: $routermac\n";

#
# We know the VM router IP, but we need an IP alias locally.
#
my $exists = `$IP address show dev $iface | fgrep '$VMALIAS'`;
if ($?) {
    print "Adding IP alias $VMALIAS to $iface\n";
    system("$IP address add ${VMALIAS}/12 dev $iface");
    if ($?) {
	fatal("Could not add alias $VMALIAS to $iface");
    }
}
#
# Need to ping the VM router so that we can ask arp for the MAC.
#
system("$PING -c 4 $VMROUTER");
if ($?) {
    fatal("Could not ping the VM router at $VMROUTER");
}
$vmroutermac = LockMac($VMROUTER);
print "VM Gateway IP: $VMROUTER, VM Gateway MAC: $vmroutermac\n";
    
#
# Let try this; send email on transition or every hour.
#
my $status = {
    "$routerip" => {
	"rogue"    => 0,
	"lastmail" => 0,
	"mac"      => lc($routermac),
    },
    "$VMROUTER" => {
	"rogue"    => 0,
	"lastmail" => 0,
	"mac"      => lc($vmroutermac),
    },
};

while (1) {
    my $mac;
    
    print "\nRunning at ".
	POSIX::strftime("20%y-%m-%d %H:%M:%S", localtime()) . "\n";

    foreach my $ip (keys(%{$status})) {
	my $info     = $status->{$ip};
	my $mac      = $info->{'mac'};
	my $rogue    = $info->{'rogue'};
	my $lastmail = $info->{'lastmail'};
	my $curmac   = GetMac($ip);
	my $canmail  = 0;

	if (!$lastmail || time() - $lastmail > 900) {
	    $canmail = 1;
	}

	if (!defined($curmac)) {
	    if ($canmail) {
		notify("Could not get the current MAC for $ip");
		$info->{'lastmail'} = time();
	    }
	    next;
	}
	print "Current MAC for $ip: $curmac\n";

	if (lc($curmac) ne $mac) {
	    if ($canmail) {
		notify("Someone has stolen the MAC for gateway $ip: $curmac");
		$info->{'lastmail'} = time();
	    }
	    $info->{'rogue'} = 1;
	}
	elsif ($rogue) {
	    notify("Rogue MAC for gateway $ip has disappeared!");
	    $info->{'rogue'} = 0;
	    $info->{'lastmail'} = 0;
	}
    }
    last
	if ($oneshot);
    
    sleep(60);
}
exit(0);

#
# Get the current mac using nmap
#
sub GetMac($)
{
    my ($ip) = @_;
    my $curmac;

    my $output = ExecQuiet("$NMAP -n -sn -e $iface $ip");
    if (!defined($output)) {
	fatal("Could start nmap, exiting.");
    }
    #
    # This is what nmap will spit out.
    # 
    # Starting Nmap 7.80 ( https://nmap.org ) at 2024-09-19 12:16 EDT
    # Nmap scan report for 130.127.132.1
    # Host is up (0.0014s latency).
    # MAC Address: F4:CC:55:66:C6:36 (Juniper Networks)
    # Nmap done: 1 IP address (1 host up) scanned in 0.08 seconds
    #
    if ($?) {
	print $output;
	return undef;
    }
    if ($debug) {
	print $output;
    }
    foreach my $line (split(/\n/, $output)) {
	if ($line =~ /^MAC Address: ((\w\w\:){5}+\w\w)/) {
	    $curmac = $1;
	    chomp($curmac);
	    last;
	}
    }
    if (!defined($curmac)) {
	print $output if (!$debug);
	print "Could not get the MAC for $ip from nmap output\n";
    }
    return $curmac;
}

#
# Get the mac for an IP, and then lock it down.
#
sub LockMac($)
{
    my ($ip) = @_;
    my $mac;

    my $arpstuff = ExecQuiet("$ARP -ane $ip");
    if ($? || !defined($arpstuff)) {
	fatal("Could not get MAC for the gateway ($ip");
    }
    foreach my $line (split(/\n/, $arpstuff)) {
	if ($line =~ /^${ip}/) {
	    my @tokens = split(/\s+/, $line);
	    my $tmp = $tokens[2];
	    if ($tmp =~ /^(\w\w\:){5}+\w\w$/) {
		$mac = $tmp;
		last;
	    }
	}
    }
    if (!defined($mac)) {
	print $arpstuff;
	fatal("Could not get determine MAC for $ip");
    }

    #
    # Lock it down in case someone steals the gateway.
    #
    system("$ARP -s $ip $mac");
    if ($?) {
	fatal("Could not arp lock down $mac for $ip");
    }
    print "MAC $mac has been locked down for $ip\n";
    return $mac;
}

sub fatal($)
{
    my($mesg) = $_[0];
    print "$mesg\n";

    if (! $impotent) {
	SENDMAIL($tbops, "Gateway Watcher at $domain", $mesg, $tbops);
    }
    exit(-1);
}

sub notify($)
{
    my($mesg) = $_[0];
    print "$mesg\n";

    if (! $impotent) {
	SENDMAIL($tbops, "Gateway Watcher at $domain", $mesg, $tbops);
    }
}

#
# Run a command, being sure to capture all output. 
#
sub ExecQuiet($)
{
    #
    # Use a pipe read, so that we save away the output
    #
    my ($command) = @_;
    my $output    = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	print STDERR "ExecQuiet Failure; popen failed!\n";
	return undef;
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}
