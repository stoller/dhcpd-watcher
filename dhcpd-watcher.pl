#!/usr/bin/perl -w
#
# Copyright (c) 2000-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use Getopt::Std;
use English;
use Errno;
use POSIX qw(strftime);

#
# Watch for rogue DHCPD servers on the control network. Has to be run from
# an experimental node since boss/ops cannot send broadcast packets when
# the cluster has a segmented network topology.
#
# See https://www.redhat.com/sysadmin/finding-rogue-devices
#
sub usage()
{
    print "Usage: dhcpd-watcher.pl [-dn]\n";
    exit(1);
}
my $optlist   = "dns";
my $debug     = 0;
my $impotent  = 0;
my $oneshot   = 0;
my $iface;
my $domain;
my $tbops;

my $NMAP      = "/bin/nmap";

#
# Turn off line buffering on output
#
$| = 1;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Load the OS independent support library. It will load the OS dependent
# library and initialize itself.
#
use libsetup;
use libtmcc;
use libtestbed;

# Protos
sub fatal($);
sub notify($);
sub ExecQuiet($);

#
# Parse command arguments. Once we return from getopts, all that should be
# left are the required arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"h"})) {
    usage();
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"s"})) {
    $oneshot++;
}

# For sending the email alerts through the local boss.
my ($bossname,$bossip) = libtmcc::tmccbossinfo();
my $opsname = $bossname =~ s/boss/ops/r;

# Figure out the control net interface for nmape
if (-e "$BOOTDIR/controlif") {
    $iface = `cat $BOOTDIR/controlif`;
    chomp($iface);
}
else {
    fatal("Could not get the control interface");
}
my $command = "$NMAP --script broadcast-dhcp-discover -e $iface";
print "$command\n" if ($debug);

if (-e "$BOOTDIR/mydomain") {
    $domain = `cat $BOOTDIR/mydomain`;
    chomp($domain);
}
else {
    fatal("Could not get the local domain");
}

# Local testbed-ops forwards to Utah
if (!$debug) {
    $tbops = "testbed-ops\@${opsname}";
}
else {
    $tbops = "stoller\@ops.emulab.net";
}

#
# Let try this; send email on transition or every hour.
#
my $rogue    = 0;
my $lastmail = 0;

while (1) {
    print "Running at ".
	POSIX::strftime("20%y-%m-%d %H:%M:%S", localtime()) . "\n";

    my $output = ExecQuiet($command);
    if (!defined($output)) {
	fatal("Could start nmap, exiting.");
    }
    if ($?) {
	if (!$lastmail || time() - $lastmail > 3600) {
	    notify($output);
	    $lastmail = time();
	}
	goto skip;
    }
    print $output;
    if ($output =~ /response/im) {
	$rogue++;
	if (!$lastmail || time() - $lastmail > 3600) {
	    notify($output);
	    $lastmail = time();
	}
    }
    elsif ($rogue) {
	notify("Rogue DHPCD server(s) have disappeared");
	$lastmail = 0;
	$rogue    = 0;
    }
    last
	if ($oneshot);
  skip:
    sleep(60);
}
exit(0);


sub fatal($)
{
    my($mesg) = $_[0];

    notify("$mesg");
    exit(-1);
}

sub notify($)
{
    my($mesg) = $_[0];

    if (! $impotent) {
	SENDMAIL($tbops, "DHCPD Watcher at $domain", $mesg, $tbops);
    }
}

#
# Run a command, being sure to capture all output. 
#
sub ExecQuiet($)
{
    #
    # Use a pipe read, so that we save away the output
    #
    my ($command) = @_;
    my $output    = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	print STDERR "ExecQuiet Failure; popen failed!\n";
	return undef;
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}
