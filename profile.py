"""Setup a daemon on an experimental node to watch for rogue DHCPD servers
and send mail to the local testbed-ops list.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab

IMAGEURN = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Define a parameter to set the node. Needs to be a URN.
pc.defineParameter("NodeID", "Optional Node ID",
                   portal.ParameterType.STRING, "")

params = pc.bindParameters()

node = request.RawPC("watcher")
node.disk_image = IMAGEURN
if params.NodeID == '':
    node.hardware_type = "pcslow"
else:
    node.component_id = params.NodeID
    pass
node.addService(pg.Execute(shell="sh", command="/local/repository/install.sh"))
node.addService(pg.Execute(shell="sh", command="/local/repository/start.sh"))

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
