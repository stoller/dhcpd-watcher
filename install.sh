#!/bin/sh

if [ -f /etc/.watchready ]; then
    exit 0
fi

#
# From David, to disable local resolver cache
#
DOMAIN=`cat /var/emulab/boot/mydomain`
BOSSIP=`cat /var/emulab/boot/bossip`

echo "nameserver $BOSSIP" > /tmp/resolv.conf
echo "search $DOMAIN" >> /tmp/resolv.conf
echo "nameserver 155.98.60.2" >> /tmp/resolv.conf

sudo systemctl stop systemd-resolved.service
sudo systemctl disable systemd-resolved.service
sudo cp -f /tmp/resolv.conf /etc/resolv.conf

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

# Accept all menu defaults
export DEBIAN_FRONTEND=noninteractive

sudo apt-get -y install mailutils daemon nmap
if [ $? -ne 0 ]; then
    echo 'apt-get install failed'
    exit 1
fi

#
# Marker that says we completed the install. 
#
sudo touch /etc/.watchready
exit 0
