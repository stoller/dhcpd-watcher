# dhcpd-watcher

Watch for rogue DHCPD servers on the control network. Has to be run from
an experimental node since boss/ops cannot send broadcast packets when
the cluster has a segmented network topology.

See https://www.redhat.com/sysadmin/finding-rogue-devices
